import inputs from "./assets/json/inputs.js";

var app = new Vue({
    el: '#app',
    data: {
      message: 'Hello Vue!',
      items: inputs,
      typeOfServices: [
        "Ден за ден",
        "Датум на достава",
        "Денес за утре"
      ],
      additionalServices: [
        {
          name: "Поштарина",
          ext: null
        },
        {
          name: "Осигурување на пратка",
          ext: "ден"
        },
        {
          name: "Повратен откуп",
          ext: "ден"
        },
        {
          name: "Провизија",
          ext: "ден"
        },
        {
          name: "Повратен документ",
          ext: null
        }
      ],
      paymants: ["ИФ", "ПФ", "ИГ", "ПГ"],
      shipments: [
        "Број на писма",
        "Број на пакети",
        "Вк. тежина(kg)",
        "Висина(m)",
        "Должина(m)",
        "Ширина(m)",
        "Вк. волумен(m³)"
      ],
      methodOfDelivery: [
        "Адреса/Адреса",
        "Адреса/Пошта",
        "Пошта/Пошта",
        "Пошта/Адреса"
      ],
      repeat: function(n) {
        return Array(n);
      }
    }
});